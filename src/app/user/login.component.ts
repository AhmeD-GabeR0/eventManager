import { Component } from '@angular/core'
import { AuthService } from './auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
})
export class LoginComponent {
    constructor(private auth: AuthService, private router: Router){}
    userName;
    password;
       login(formvalue) {
         this.auth.loginUser(formvalue.userName,formvalue.password)
        this.router.navigate(['events'])
       }
       cancel(){
           this.router.navigate(['events'])
       }
       
}