import { Component, OnInit } from '@angular/core'
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Component({
  templateUrl:'./profile.component.html',
  styles: [`
    em {float:right; color:#E05c65; padding-left:10px;} 
    .error input {background-color:#E3c3c5}
  `]
})
export class ProfileComponent implements OnInit {
  constructor(private auth: AuthService, private router: Router){}
  profileForm: FormGroup;
  ngOnInit(): void {
    let firstName = new FormControl
    (this.auth.currentUser.firstName, [Validators.required,
    Validators.pattern('[a-zA-Z].*')]);
    let lastName = new FormControl
    (this.auth.currentUser.lastName, [Validators.required,
    Validators.pattern('[a-zA-Z].*')]);
    this.profileForm = new FormGroup({
      firstName: firstName,
      lastName: lastName
    })
  }
  cancel(){
    this.router.navigate(['events'])
  }
  editProfile (formValues) {
    if(this.profileForm.valid){
      this.auth.editUserProfile(formValues.firstName, formValues.lastName)
      this.router.navigate(['events'])
    }
  }

  validClassFirstName () {
    return this.profileForm.controls.firstName.valid || this.profileForm.controls.firstName.untouched;

  }
  validClassLastName () {
    
      return this.profileForm.controls.lastName.valid || this.profileForm.controls.lastName.untouched;
    

  }
}