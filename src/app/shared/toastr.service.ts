import {Injectable} from '@angular/core'
declare let toastr
@Injectable()
export class ToastrService {
    
    sucsess(massage: string, title?: string){
        toastr.sucsess(massage, title)
    }
    info(massage: string, title?: string){
        toastr.info(massage, title)
    }
    warinig(massage: string, title?: string){
        toastr.warinig(massage, title)
    }
    error(massage: string, title?: string){
        toastr.error(massage, title)
    }
}