import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {EventsListComponent, 
  EventRouteActivator, 
  CreateNewEventComponent, 
  EventDetailsComponent,
  EventService,
  EventsThumbnail,
  EventListResolver,
  CreateSesssionComponent,
  SessionListComponent
} from './index'
import { EventsAppComponent } from './events-app.component';
import { NavBarComponent } from './navbar/nav-bar.component';
import { ToastrService } from './shared/toastr.service';
import {appRoute} from './routes'
import { RouterModule, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Error404Component } from './error/404.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './user/auth.service';

@NgModule({
  declarations: [
    EventsAppComponent,
    EventsListComponent,
    EventsThumbnail,
    NavBarComponent,
    EventDetailsComponent,
    CreateNewEventComponent,
    Error404Component,
    CreateSesssionComponent,
    SessionListComponent

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoute)
  ],
  providers: [EventService, ToastrService,EventRouteActivator, EventListResolver, AuthService],
  bootstrap: [EventsAppComponent]
})
export class AppModule { }
