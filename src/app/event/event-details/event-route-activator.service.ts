import { CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { EventService } from '../../shared/events.service';

@Injectable()
export class EventRouteActivator implements CanActivate {

  constructor(private eventService: EventService, private router: Router) {

  }
  canActivate(route: ActivatedRouteSnapshot) {
    const existsed = !!this.eventService.getEvent(+route.params['id'])
    if (!existsed) {
      this.router.navigate(['/404']);
    }
    return existsed;
  }
}