import { Component, OnInit } from '@angular/core'
import { EventService } from '../../shared/events.service';
import { ToastrService } from '../../shared/toastr.service';
import {ActivatedRoute} from '@angular/router';
import {IEvent, ISessions} from '../../shared/index';

@Component({
  selector: 'events-details',
  templateUrl: './event-details.component.html',
  styles: [`
  .container {padding-left:20px; padding-right:20px}
  .event-image {height:100px}
  a {cursor:pointer}`]

})

export class EventDetailsComponent implements OnInit {
 event:IEvent
 addMode: boolean;
  constructor(private eventService: EventService, private toastrService: ToastrService, private route: ActivatedRoute) { }

  ngOnInit() {
   this.event = this.eventService.getEvent(+this.route.snapshot.params['id'])
  }
  addSession(){
    this.addMode = true
  }

  saveNewSession(session:ISessions){
    const nextId = Math.max.apply(null,this.event.sessions.map(s=>s.id));
    session.id = nextId;
    this.event.sessions.push(session)
    this.eventService.updateEvent(this.event);
    this.addMode = false;

  }
  cancel(){
    this.addMode = false
  }


}