import {Component} from '@angular/core';
import { Router } from '@angular/router';
import { EventService } from 'src/app/shared';

@Component({
    selector: 'new-event',
    templateUrl: './create-new-event.component.html',
    styles: [`
    em {float:right; color:#E05c65; padding-left:10px;} 
    .error input {background-color:#E3c3c5}
  `]
})
export class CreateNewEventComponent {
    constructor(private router:Router, private eventService: EventService) {}
    newEvent;
    navigateToevents() {
        
    }
    saveEvent(formValues) {
        console.log(formValues)
        this.eventService.saveEvent(formValues)
        this.router.navigate(['/events'])
    }
}