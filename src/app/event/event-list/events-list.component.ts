import { Component, OnInit } from '@angular/core'
import { EventService } from '../../shared/events.service';
import { ToastrService } from '../../shared/toastr.service';
import {ActivatedRoute} from '@angular/router'
import {IEvent} from '../../shared/index';


@Component({
  selector: 'events-list',
  templateUrl: './events-list.component.html'
})

export class EventsListComponent implements OnInit {
  
  events:IEvent[];
  constructor(private eventService: EventService, private toastrService: ToastrService, private route:ActivatedRoute) { 

  }

  ngOnInit() {
  this.eventService.getEvents().subscribe(events => {this.events = events});
  }
  handelToaster(eventName){
   this.toastrService.info(eventName)
  }

}