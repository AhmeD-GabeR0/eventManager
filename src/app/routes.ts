import {Routes} from '@angular/router';
import { Error404Component } from './error/404.component';
import { EventRouteActivator } from './event/event-details/event-route-activator.service';
import {EventsListComponent,
    EventDetailsComponent,
    CreateNewEventComponent,
    EventListResolver,
    CreateSesssionComponent} from './index'

export const appRoute: Routes = [
    {path: 'events/new', component:CreateNewEventComponent},
    {path: 'events', component:EventsListComponent},
    {path: 'events/:id', component:EventDetailsComponent, canActivate:[EventRouteActivator]},
    {path: 'events/sessions/new', component: CreateSesssionComponent},
    {path:'404', component:Error404Component},
    {path: '', redirectTo:'/events', pathMatch:'full'},
    {path: 'user', loadChildren:'./user/user.module#UserModule'}
]

